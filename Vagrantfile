# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrant file used to run 3 VMs - one database server and two web servers (customer website, admin website)
Vagrant.configure("2") do |config|
 
  # All servers will run Ubuntu
  config.vm.box = "ubuntu/xenial64"

  # Automatically check for box updates on vagrant up
  config.vm.box_check_update = true

   # Sets up a VM for hosting a admin web interface
  config.vm.define "awebserver" do |awebserver|

    # Sets the name of the server
    awebserver.vm.hostname = "awebserver"
    
    # Sets up port forwarding so that the host computer can connect to IP address 127.0.0.1 port 8080, 
    # and that network request will reach our webserver VM's port 80.
    awebserver.vm.network "forwarded_port", guest: 80, host: 8083, host_ip: "127.0.0.1"
    
    # Sets up a private network that our VMs will use to communicate and assigns an ip
    # for the VM on the private network
    awebserver.vm.network "private_network", ip: "192.168.2.12"

    # Took this from the lab work
    awebserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]

    # Specifies the shell commands to provision the cwebserver VM. 
    awebserver.vm.provision "shell", inline: <<-SHELL
      # update the packages
      echo "Update package lists"
      apt-get update
      apt-get install curl
            
      echo "Install node"
      
      curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
      sudo apt-get install -y nodejs
      sudo apt-get install -y build-essential
      echo "---- here ----"

      echo "Set the vagrant user as the owner of files in /project"
      # sudo chown -R vagrant web/admin

    SHELL
    awebserver.vm.provision "file", source: "./web/admin", destination: "$HOME/web/admin"
  end

  # Sets up a VM for hosting a customer web interface
  config.vm.define "cwebserver" do |cwebserver|

    # Sets the name of the server
    cwebserver.vm.hostname = "cwebserver"
    
    # Sets up port forwarding so that the host computer can connect to IP address 127.0.0.1 port 8082, 
    # and that network request will reach our webserver VM's port 80.
    cwebserver.vm.network "forwarded_port", guest: 80, host: 8082, host_ip: "127.0.0.1"
    
    # Sets up a private network that our VMs will use to communicate and assigns an ip
    # for the VM on the private network
    cwebserver.vm.network "private_network", ip: "192.168.2.11"

    # Took this from the lab work
    cwebserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]

    # Specifies the shell commands to provision the cwebserver VM. 
    cwebserver.vm.provision "shell", inline: <<-SHELL
       # update the packages
      echo "Update package lists"
      apt-get update
      apt-get install curl
            
      echo "Install node"
      
      curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
      sudo apt-get install -y nodejs
      sudo apt-get install -y build-essential
      echo "---- here ----"

      echo "Set the vagrant user as the owner of files in /project"
      # sudo chown -R vagrant customer.conf
    SHELL
    cwebserver.vm.provision "file", source: "./web/customer", destination: "$HOME/web/customer"
  end

  # Sets up a VM for hosting a database server 
  config.vm.define "dbserver" do |dbserver|
    
    # Set the name of the server
    dbserver.vm.hostname = "dbserver"
    
    # Assign the ip for the VM on the private network
    dbserver.vm.network "private_network", ip: "192.168.2.13"

    # Taken from the lab work
    dbserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]
    
    dbserver.vm.provision "shell", inline: <<-SHELL
      # Update Ubuntu software packages.
      sudo apt-get update
      
      # We create a shell variable MYSQL_PWD that contains the MySQL root password
      export MYSQL_PWD='db_admin'

      # Give the password to the installer first before installing mysql so we don't get stopped and asked for the password
      echo "mysql-server mysql-server/root_password password $MYSQL_PWD" | debconf-set-selections 
      echo "mysql-server mysql-server/root_password_again password $MYSQL_PWD" | debconf-set-selections

      # Install the MySQL database server.
      sudo apt-get -y install mysql-server

      # restart the sql service
   #   sudo service mysql restart


      # Create a database.
      echo "CREATE DATABASE IF NOT EXISTS checkoutmate;" | mysql

      # Create an admin user with the given password.
      echo "CREATE USER IF NOT EXISTS 'db_admin'@'%' IDENTIFIED BY 'db_admin';" | mysql

      # Provide all privilieges to the user created above
      echo "GRANT ALL PRIVILEGES ON checkoutmate.* TO 'db_admin'@'%'" | mysql
      
      # Set the MYSQL_PWD shell variable that the mysql command will
      # try to use as the database password
      export MYSQL_PWD='db_admin'

      # Run the initial sql script
      # The mysql command specifies both the user to connect as (admin) and the database to use (checkoutmate).
      cat /vagrant/database-setup.sql | mysql -u db_admin checkoutmate

      # Update the mysql .cnf file to accept connections from any network,
      # not just from the local machine i.e. accept connections from the other VM's
      sed -i'' -e '/bind-address/s/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf

      # Restart the MySQL server to ensure that it picks up our configuration changes.
      sudo service mysql restart
    SHELL
  end

end
