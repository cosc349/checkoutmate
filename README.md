# Checkout Mate #

### Overview ###
We have extended CheckoutMate, the app we developed for Assignment One. 
Our app addresses the problem of vulnerable people having accessibility issues for essential services during lockdowns or while self isolating, 
in this case supermarket shopping, by matching people in need with people who can help. The major changes to the app itself are the displaying of 
user data for the site administrator in order to see how people can be paired together, a form to make those matches, and UI improvements. 
The deployment of CheckoutMate is now via two Docker containers - one for the client server and one for the admin server - and for data 
storage we used MySQL with an Amazon Relational Database Service.

### New technologies that we used for Assignment 2 ###
• Docker

• Container

• Relation Database Service (RDS)

### How to run the application ###
• Install the latest version of Vagrant which can be found at the following link

 https://www.vagrantup.com/downloads

• Install the latest version of VirtualBox which can be found at the following link 

https://www.virtualbox.org/wiki/Downloads

• Following this, the repository to our project need to be cloned from the link below using the command in terminal 

 git clone https://bitbucket.org/cosc349/checkoutmate

• Once cloned, navigate through the terminal into the directory of the repository. Once there, use the following command in the terminal to start the virtual machines.

vagrant up --provider virtualbox

• To access the customer website, enter the following into a command prompt:

vagrant ssh cwebserver

cd web/customer

npm install && npm run start

• Then navigate in a web browser to:

http://192.168.2.11:8082/

• To access the admin website, enter the following into a command prompt:

vagrant ssh awebserver

cd web/admin

npm install 

npm run start

• Then, navigate to the following address in a web browser:

http://192.168.2.12:8083/




