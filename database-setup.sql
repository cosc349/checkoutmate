DROP TABLE client;
DROP TABLE volunteer;
DROP TABLE user;
DROP TABLE webadmin;

--CREATE DATABASE IF NOT EXISTS checkoutmate;

CREATE TABLE IF NOT EXISTS user (      -- ALL THE USERS PERSONAL INFORMATION
  username varchar(50) NOT NULL,       
  person_name varchar(50) NOT NULL,
  email_address varchar(100) NOT NULL,
  phone_number varchar(50) NOT NULL,
  user_password varchar(255) NOT NULL,
  PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS client (       -- CLIENTS WHO NEED HELP
  username varchar(50) NOT NULL,          -- username as the PK and the FK from the user table 
  suburb varchar(50) DEFAULT NULL,
  match_day varchar(50) DEFAULT NULL,
  client_vol varchar(50) DEFAULT NULL,
  -- comments varchar(150),
  PRIMARY KEY (username),
  CONSTRAINT username FOREIGN KEY (username) REFERENCES user(username)

);

CREATE TABLE IF NOT EXISTS volunteer (       -- CLIENTS WHO NEED HELP
  username varchar(50) NOT NULL,          -- username as the PK and the FK from the user table 
  suburb varchar(50) DEFAULT NULL,
  match_day varchar(50) DEFAULT NULL,
  -- comments varchar(150),
  PRIMARY KEY (username),
  CONSTRAINT usernme FOREIGN KEY (username) REFERENCES user(username)

);

CREATE TABLE IF NOT EXISTS matches(
  volunteer varchar(50) NOT NULL,
  client varchar(50) NOT NULL,
  suburb varchar(50) NOT NULL,
  match_date varchar(50) NOT NULL,
  PRIMARY KEY (volunteer, client)
);

CREATE TABLE IF NOT EXISTS webadmin (
  username varchar(50) NOT NULL,
  admin_password varchar(50) NOT NULL,
  PRIMARY KEY (username)
);

-- INSERT INTO accounts (id, username, account_password, email_address) VALUES (1, 'test', 'test', 'test@test.com');
-- INSERT INTO user VALUES('test1', 'test', 'test', 'test@test.com', '1234', 'test', 'testlane', '2017-06-15', 'female');
INSERT INTO user VALUES("ahmma182", "Maaha Ahmad", "maaha@gmail.com", "021345", "maaha");
INSERT INTO user VALUES("jemimalomax", "Jemima LS", "jemima@gmail.com", "567345", "jemima");
INSERT INTO user VALUES("testname", "Test Name", "testname", "567394", "testnme");
INSERT INTO user VALUES("testvol", "Test Vol", "testvol", "678444", "testvol");
INSERT INTO user VALUES("testlog", "Test Log", "testlog", "456333", "testlog");
INSERT INTO user VALUES("testcl", "Test Client", "testcl", "432345", "testcl");
INSERT INTO client VALUES("ahmma182", "North Dunedin", "Saturday", "Client");
INSERT INTO client VALUES("testname", "St Clair", "Tuesday", "Volunteer");
INSERT INTO client VALUES("testvol",  "Mosgiel", "Tuesday", "Volunteer");
INSERT INTO client VALUES("testlog", "North Dunedin", "Saturday", "Volunteer");
INSERT INTO client VALUES("testcl", "Mosgiel", "Tuesday", "Client");
INSERT INTO matches VALUES("vol_1", "client_1", "Mosgiel", "Tuesday");
INSERT INTO matches VALUES("vol_2", "client_2", "St Clair", "Wednesday");

INSERT INTO volunteer VALUES("jemimalomax", "South Dunedin", "Sunday");
INSERT INTO webadmin VALUES("admin", "admin");