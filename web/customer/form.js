var http = require('http');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-Parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var url = require('url');
var fs = require('fs');
const { response } = require('express');
var app = express();
var router = express.Router();
var jsdom = require('jsdom');
var $ = require('jquery')(new jsdom.JSDOM().window);

// view engin setup
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

let con = mysql.createConnection({
  host: "192.168.2.13",
  user: "db_admin",
  port: "8082",
  password: "db_admin",
  database: "checkoutmate"
});


con.connect(function(err){
  if (err) throw err;
  console.log("Connected!");
});
/**module.exports = {
  test: function (req, res){
    //console.log('/requestHelp.html');
   // res.redirect('/Volunteer.html');
   // res.redirect('/requestHelp.html');
  }
};*/

router.get('/');

// link html page to insert the data into need help table 
$("NH").click(function(){
  console.log('click button');
  $.ajax({url:'/requestHelp.html', success:function(res){
    console.log('respnse', res);
}});
});
function buttonaction(res){
  res.send('ok');
}
router.get('/requestHelp.html', function (req, res) {
  buttonaction(res);
});
// link html page to insert the data into volunteer table 


// link to front.html file 
app.get('/', function(request, response){
  response.sendFile(path.join(__dirname + '/front.html'));
});
app.get('/Volunteer.html', function(request, response){
  response.sendFile(path.join(__dirname + '/Volunteer.html'));
});

app.get('/requestHelp.html', function(request, response){
  response.sendFile(path.join(__dirname + '/requestHelp.html'));
});



app.post('/Volunteer', function(req, res, next) {
  var suburb = req.body.suburb;
  var match_day = req.body.days;
  
  let sql = "INSERT INTO volunteer (suburb, match_day) VALUES (suburb, match_day)";
   con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
    
});
res.redirect('/Volunteer.html');
});


app.post('/request', function(req, res, next) {
  var suburb = req.body.suburb;
  var match_day = req.body.days;
  
  let sql = "INSERT INTO volunteer (suburb, match_day) VALUES (suburb, match_day)";
   con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
    
});
res.redirect('/requestHelp.html');
});

/**catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});*/
 
app.listen(8082, function () {
    console.log('server running on port 8081');
});