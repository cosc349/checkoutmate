var mysql = require('mysql');
var express = require('express')
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var http = require('http');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var url = require('url');
var fs = require('fs');
var jsdom = require('jsdom');
var $ = require('jquery')(new jsdom.JSDOM().window);

var connection = mysql.createConnection({
    host : 'checkoutmate.cxbxyzniesyi.us-east-1.rds.amazonaws.com',
    port: '3306',
    user : 'db_admin',
    password : 'db_admin',
    database : 'checkoutmate',
})

connection.connect();

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(request, response){
    response.sendFile(path.join(__dirname + '/login.html'));

});

app.get('/design.css', function(request, response){
    response.sendFile(path.join(__dirname + '/design.css'));

});

app.get('/front', function(request, response){
    response.sendFile(path.join(__dirname + '/front.html'));
  });
  app.get('/Vol', function(request, response){
    response.sendFile(path.join(__dirname + '/Volunteer.html'));
  });
  
  app.get('/requestHelp', function(request, response){
    response.sendFile(path.join(__dirname + '/requestHelp.html'));
  });

  
app.post('/Volunteer', function(req, res, next) {
    inputData={
        username: req.session.username,
        suburb: req.body.suburbs,
        match_day: req.body.days

    }
    var sql = 'INSERT INTO volunteer SET ?';

    connection.query(sql, inputData, function(err, data){
        console.log(err);
    res.redirect('/Vol');
        
});
 });
  
  app.post('/userInfo', function(req, res, next) {
      inputData={
          username: req.session.username,
          //person_name: req.session.person_name,
          suburb: req.body.suburbs,
          match_day: req.body.days,
          client_vol: req.body.submissiontype
      } 
    var sql = 'INSERT INTO client SET ?';

    connection.query(sql, inputData, function(err, data){
        console.log(err);
  res.redirect('/requestHelp');
  });
});

app.post('/register', function(request, response){
inputData={
    username: request.body.usnme,
    person_name: request.body.name,
    email_address: request.body.email,
    phone_number: request.body.phone,
    user_password: request.body.pwd

} 
var sql = 'INSERT INTO user SET ?';
connection.query(sql, inputData, function(err, data){
        console.log(err);
    
        response.send('You are successfully registered!');
        response.end();
});
});


app.post('/auth', function(request, response){
    var username = request.body.username;
    //var person_name = request.body.person_name;
    var password = request.body.password;
    if(username && password){
        connection.query('SELECT * FROM user WHERE username = ? AND user_password = ?',
        [username, password], function(error, results, fields){
            console.log(error);
            if(results.length > 0){
                request.session.loggedin = true;
                request.session.username = username;
                //request.session.person_name = person_name;
                response.redirect('/front');
            }else {
                response.send('Incorrect username or password');
            } 
            response.end();
            
            
        });

    } else {
        response.send('Please enter username and password');
        response.end();
  
    }
});

app.get('/home', function(request, response){
    if(request.session.loggedin){
        response.send('Welcome back, ' + request.session.username + '!');
    } else{
        response.send('Please login to view this page!');
    }
    response.end();
});

app.listen(8082);


