var mysql = require('mysql');
var express = require('express')
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var http = require('http');
var cookieParser = require('cookie-Parser');
var logger = require('morgan');
var url = require('url');
var fs = require('fs');
var jsdom = require('jsdom');
var $ = require('jquery')(new jsdom.JSDOM().window);

var connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'password',
    database : '',
})

connection.connect();

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(request, response){
    response.sendFile(path.join(__dirname + '/login.html'));

});

app.get('/design.css', function(request, response){
    response.sendFile(path.join(__dirname + '/design.css'));

});

app.get('/front', function(request, response){
    response.sendFile(path.join(__dirname + '/front.html'));
  });
  app.get('/Vol', function(request, response){
    response.sendFile(path.join(__dirname + '/Volunteer.html'));
  });
  
  app.get('/requestHelp', function(request, response){
    response.sendFile(path.join(__dirname + '/requestHelp.html'));
  });

  
app.post('/Volunteer', function(req, res, next) {
    inputData={
        username: req.session.username,
        suburb: req.body.street,
        day: req.body.day

    }
    var sql = 'INSERT INTO `volunteer` SET ?';

    connection.query(sql, inputData, function(err, data){
        console.log(err);
    res.redirect('/Vol');
        
});
 });
  
  app.post('/request', function(req, res, next) {
      inputData={
          username: req.session.username,
          suburb: req.body.street,
          day: req.body.day
      } 
    var sql = 'INSERT INTO `requestHelp` SET ?';

    connection.query(sql, inputData, function(err, data){
        console.log(err);
  res.redirect('/requestHelp');
  });
});

app.post('/register', function(request, response){
inputData={
    name: request.body.name,
    phone: request.body.phone,
    email: request.body.email,
    username: request.body.usnme,
    password: request.body.pwd

} 
var sql = 'INSERT INTO accounts SET ?';
connection.query(sql, inputData, function(err, data){
        console.log(err);
    
        response.send('You are successfully registered!');
        response.end();
});
});


app.post('/auth', function(request, response){
    var username = request.body.username;
    var password = request.body.password;
    if(username && password){
        connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?',
        [username, password], function(error, results, fields){
            console.log(error);
            if(results.length > 0){
                request.session.loggedin = true;
                request.session.username = username;
                response.redirect('/front');
            }else {
                response.send('Incorrect username or password');
            } 
            response.end();
            
            
        });

    } else {
        response.send('Please enter username and password');
        response.end();
  
    }
});

app.get('/home', function(request, response){
    if(request.session.loggedin){
        response.send('Welcome back, ' + request.session.username + '!');
    } else{
        response.send('Please login to view this page!');
    }
    response.end();
});

app.listen(8081);


